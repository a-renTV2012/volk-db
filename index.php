<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <title>Исполнители</title>
    <link rel="stylesheet" href="css/bootstrap-grid.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
  </head>

  <body>
    <div class="container">
      <div class="row justify-content-center header">
        <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10 wrap">
          <h1 class="heading">БАЗА ДАННЫХ ИСПОЛНИТЕЛЕЙ</h1>
        </div>
      </div>

      <div class="row justify-content-between bar">
        <div class="col-8 col-sm-8 col-md-5 col-lg-5 col-xl-5 wrap">
          <h3 class="greeting">Добро пожаловать!</h3>
        </div>

        <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 wrap">
          <button class="add">Добавить</button>
        </div>

        <div class="col-4 col-sm-4 col-md-3 col-lg-3 col-xl-3 offset-8 offset-sm-8 offset-md-0 offset-lg-0 offset-xl-0 wrap">
          <button class="search">Найти</button>
        </div>
      </div>
    </div>

    <div class="overlay">
      <div class="overlay-delete">
        <div class="overlay-delete-title">
          <h2 class="overlay-delete-title-text">УДАЛИТЬ ЗАПИСЬ</h2>
        </div>

        <form class="overlay-delete-form">
          <div class="row dustify-content-center wrap">
            <div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 elements">
              <input class="password" name="password" type="password" placeholder="Пароль">

              <button class="submit">Удалить</button>
            </div>
          </div>
        </form>

        <div class="overlay-delete-close">&times;</div>
      </div>

      <div class="overlay-desc">
        <div class="overlay-desc-title">
          <h2 class="overlay-desc-title-text">ИЗМЕНИТЬ ОПИСАНИЕ</h2>
        </div>

        <form class="overlay-desc-form">
          <div class="row dustify-content-center wrap">
            <div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 elements">
              <textarea name="description" class="description" id="description">Введите дополнительную информацию</textarea>
              <input class="password" name="password" type="password" placeholder="Пароль">
              <button class="submit">Сохранить</button>
            </div>
          </div>
        </form>

        <div class="overlay-desc-close">&times;</div>
      </div>

      <div class="overlay-add">
        <div class="overlay-add-title">
          <h2 class="overlay-add-title-text">ДОБАВИТЬ ЗАПИСЬ</h2>
        </div>

        <form class="overlay-add-form">
          <div class="row justify-content-center wrap">
            <div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 elements">
              <div class="prices-add">
                <div class="title">
                  <h2 class="text">ЦЕНЫ НА УСЛУГИ</h2>
                </div>
                <div class="left">
                  <label><input type="checkbox" name="tire-fitting" id="tire_fitting-add">&nbsp;Шиномонтаж</label><br>
                  <label><input type="checkbox" name="petrol-tire_delivery" id="petrol-tire_delivery-add">&nbsp;Подвоз бензина и шин</label><br>
                  <label><input type="checkbox" name="lighter" id="lighter-add">&nbsp;Прикуриватель</label><br>
                  <label><input type="checkbox" name="auto-electrician" id="auto-electrician-add">&nbsp;Автоэлектрик</label><br>
                  <label><input type="checkbox" name="towing" id="towing-add">&nbsp;Буксировка</label><br>
                  <label><input type="checkbox" name="tow-truck" id="tow-truck-add">&nbsp;Эвакуатор</label><br>
                  <label><input type="checkbox" name="locks-breaking" id="locks-breaking-add">&nbsp;Взлом замков</label><br>
                </div>

                <div class="right">
                  <p><input type="text" name="tire_fitting_price" placeholder="5000">р</p>
                  <p><input type="text" name="petrol-tire_delivery_price" placeholder="5000">р</p>
                  <p><input type="text" name="lighter_price" placeholder="5000">р</p>
                  <p><input type="text" name="auto-electrician_price" placeholder="5000">р</p>
                  <p><input type="text" name="towing_price" placeholder="5000">р</p>
                  <p><input type="text" name="tow-truck_price" placeholder="5000">р</p>
                  <p><input type="text" name="locks-breaking_price" placeholder="5000">р</p>
                </div>

                <button class="save-add" type="button">Сохранить</button>
              </div>
              <input class="executor" name="surname" type="text" placeholder="Фамилия">
              <input class="executor" name="name" type="text" placeholder="Имя" required>
              <input class="executor last" name="patronymic" type="text" placeholder="Отчество"><br>
              <input class="phone" name="phone" type="text" placeholder="Телефон" required><br>

              <div class="left">
                <label for="whatsapp"><input class="whatsapp" type="checkbox" name="whatsapp" id="whatsapp-add">&nbsp;What's App</label><br>
                <p>Район:</p>
                <input class="district" name="district" type="text" placeholder="Кировский, ..." required><br>
              </div>

              <div class="right">
                <p>Выходные:</p>
                <input class="holidays" type="text" name="holidays" placeholder="Вторник, среда">
              </div>

              <div class="time">
                <p>Время работы:</p>
                <p>от <input type="time" name="time_start" class="start"> до <input type="time" name="time_end" class="end"></p>
              </div>

              <input class="password" name="password" type="password" placeholder="Пароль">
              <div class="buttons">
                <button class="price-add" type="button">Цены</button>
                <button class="submit">Добавить</button>
              </div>
            </div>
          </div>
        </form>

        <div class="overlay-add-close">&times;</div>
      </div>

      <div class="overlay-search">
        <div class="overlay-search-title">
          <h2 class="overlay-search-title-text">НАЙТИ ЗАПИСИ</h2>
        </div>

        <form class="overlay-search-form">
          <div class="row justify-content-center wrap">
            <div class="col-10 col-sm-10 col-md-6 col-lg-6 col-xl-6 elements">
              <p>Район:</p>
              <input class="district" name="district" type="text" placeholder="Ленинский" required><br>

              <div class="action"><label><input type="checkbox" name="tire_fitting" id="tire_fitting-search">&nbsp;Шиномонтаж</label><br></div>
              <div class="action"><label><input type="checkbox" name="petrol-tire_delivery" id="petrol-tire_delivery-search">&nbsp;Подвоз бензина и шин</label><br></div>
              <div class="action"><label><input type="checkbox" name="lighter" id="lighter-search">&nbsp;Прикуриватель</label><br></div>
              <div class="action"><label><input type="checkbox" name="auto-electrician" id="auto-electrician-search">&nbsp;Автоэлектрик</label><br></div>
              <div class="action"><label><input type="checkbox" name="towing" id="towing-search">&nbsp;Буксировка</label><br></div>
              <div class="action"><label><input type="checkbox" name="tow-truck" id="tow-truck-search">&nbsp;Эвакуатор</label><br></div>
              <div class="action"><label><input type="checkbox" name="locks-breaking" id="locks-breaking-search">&nbsp;Взлом замков</label><br></div>

              <button class="submit">Искать</button>
            </div>
          </div>
        </form>

        <div class="overlay-search-close">&times;</div>
      </div>

      <div class="overlay-change">
        <div class="overlay-change-title">
          <h2 class="overlay-change-title-text">ПРАВИТЬ ЗАПИСЬ</h2>
        </div>

        <form class="overlay-change-form">
          <div class="row justify-content-center wrap">
            <div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 elements">
              <div class="prices-change">
                <div class="title">
                  <h2 class="text">ЦЕНЫ НА УСЛУГИ</h2>
                </div>
                <div class="left">
                  <label><input type="checkbox" name="tire_fitting" id="tire_fitting-change">&nbsp;Шиномонтаж</label><br>
                  <label><input type="checkbox" name="petrol-tire_delivery" id="petrol-tire_delivery-change">&nbsp;Подвоз бензина и шин</label><br>
                  <label><input type="checkbox" name="lighter" id="lighter-change">&nbsp;Прикуриватель</label><br>
                  <label><input type="checkbox" name="auto-electrician" id="auto-electrician-change">&nbsp;Автоэлектрик</label><br>
                  <label><input type="checkbox" name="towing" id="towing-change">&nbsp;Буксировка</label><br>
                  <label><input type="checkbox" name="tow-truck" id="tow-truck-change">&nbsp;Эвакуатор</label><br>
                  <label><input type="checkbox" name="locks-breaking" id="locks-breaking-change">&nbsp;Взлом замков</label><br>
                </div>

                <div class="right">
                  <p><input type="text" name="tire_fitting_price" id="tire_fitting_price" placeholder="5000">р</p>
                  <p><input type="text" name="petrol-tire_delivery_price" id="petrol-tire_delivery_price" placeholder="5000">р</p>
                  <p><input type="text" name="lighter_price" id="lighter_price" placeholder="5000">р</p>
                  <p><input type="text" name="auto-electrician_price" id="auto-electrician_price" placeholder="5000">р</p>
                  <p><input type="text" name="towing_price" id="towing_price" placeholder="5000">р</p>
                  <p><input type="text" name="tow-truck_price" id="tow-truck_price" placeholder="5000">р</p>
                  <p><input type="text" name="locks-breaking_price" id="locks-breaking_price" placeholder="5000">р</p>
                </div>

                <button class="save-change" type="button">Сохранить</button>
              </div>
              <input class="executor" id="change-surname" name="surname" type="text" placeholder="Фамилия">
              <input class="executor" id="change-name" name="name" type="text" placeholder="Имя" required>
              <input class="executor last" id="change-patronymic" name="patronymic" type="text" placeholder="Отчество"><br>
              <input class="phone" type="text" placeholder="Телефон" required><br>

              <div class="left">
                <label for="whatsapp"><input class="whatsapp" type="checkbox" name="whatsapp" id="whatsapp-change">&nbsp;What's App</label><br>
                <p>Район:</p>
                <input class="district" type="text" placeholder="Кировский, ..." required><br>
              </div>

              <div class="right">
                <p>Выходные:</p>
                <input class="holidays" type="text" placeholder="Вторник, среда">
              </div>

              <div class="time">
                <p>Время работы:</p>
                <p>от <input type="time" class="start"> до <input type="time" class="end"></p>
              </div>

              <input class="password" name="password" type="password" placeholder="Пароль">
              <div class="buttons">
                <button class="price-change" type="button">Цены</button>
                <button class="submit">Отправить</button>
              </div>
            </div>
          </div>
        </form>

        <div class="overlay-change-close">&times;</div>
      </div>
    </div>

    <script src="js/jquery-3.3.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-3.3.1.min.js"><\/script>')</script>

    <script>
      $(document).ready(function() {
        
        $(document).on("click", ".delete", function() {
          $(".overlay").show();
          $(".overlay-delete").show();
          $(".overlay-desc").hide();
          $(".overlay-add").hide();
          $(".overlay-search").hide();
          $(".overlay-change").hide();

          record_id = $(this).attr('id');
        });

        $(document).on("click", ".text", function() {
          $(".overlay").show();
          $(".overlay-delete").hide();
          $(".overlay-desc").show();
          $(".overlay-add").hide();
          $(".overlay-search").hide();
          $(".overlay-change").hide();

          record_id = $(this).attr('id');

          $.ajax({
            type: 'POST',
            url: 'php/scripts/select-description.php',
            data: {id: record_id.replace(/[^\d]/g, '')}
          }).done(function(response) {
            if (response == '')
              $('.overlay-desc-form .description').val('Введите дополнительную информацию');
            else
              $('.overlay-desc-form .description').val(response);
          });
        });

        $(document).on("click", ".add", function() {
          $(".overlay").show();
          $(".overlay-delete").hide();
          $(".overlay-desc").hide();
          $(".overlay-add").show();
          $(".overlay-search").hide();
          $(".overlay-change").hide();
        });

        $(document).on("click", ".search", function() {
          $(".overlay").show();
          $(".overlay-delete").hide();
          $(".overlay-desc").hide();
          $(".overlay-search").show();
          $(".overlay-add").hide();
          $(".overlay-change").hide();
        });

        $(document).on("click", ".edit", function() {
          $(".overlay").show();
          $(".overlay-delete").hide();
          $(".overlay-desc").hide();
          $(".overlay-change").show();
          $(".overlay-search").hide();
          $(".overlay-add").hide();

          record_id = $(this).attr('id');

          $.ajax({
            type: 'POST',
            url: 'php/scripts/select-info.php',
            data: {id: record_id.replace(/[^\d]/g, '')}
          }).done(function(response) {
            info = JSON.parse(response);

            $('form').trigger('reset');
            $('#tire_fitting-change').removeAttr("checked");
            $('#petrol-tire_delivery-change').removeAttr("checked");
            $('#lighter-change').removeAttr("checked");
            $('#auto-electrician-change').removeAttr("checked");
            $('#towing-change').removeAttr("checked");
            $('#tow-truck-change').removeAttr("checked");
            $('#locks-breaking-change').removeAttr("checked");

            $('#change-surname').val(info.surname);
            $('#change-name').val(info.name);
            $('#change-patronymic').val(info.patronymic);
            $('.overlay-change-form .phone').val(info.phone);
            if (info.wa_flag == 1)
              $('#whatsapp-change').attr('checked', 'checked');
            if (!info.districts == false)
              $('.overlay-change-form .district').val(info.districts);
            else
            $('.overlay-change-form .district').val('');
            if (!info.holidays == false)
              $('.overlay-change-form .holidays').val(info.holidays);
            else
              $('.overlay-change-form .holidays').val('');
            $('.overlay-change-form .start').val(info.time_start);
            $('.overlay-change-form .end').val(info.time_end);

            $.each(info.services, function(index, value) {
              switch(index) {
                case 'Шиномонтаж': 
                  $('#tire_fitting-change').attr('checked', 'checked');
                  $('#tire_fitting_price').val(value);
                  break;
                
                case 'Подвоз бензина и шин': 
                  $('#petrol-tire_delivery-change').attr('checked', 'checked');
                  $('#petrol-tire_delivery_price').val(value);
                  break;
                
                case 'Прикуриватель': 
                  $('#lighter-change').attr('checked', 'checked');
                  $('#lighter_price').val(value);
                  break;
                
                case 'Автоэлектрик': 
                  $('#auto-electrician-change').attr('checked', 'checked');
                  $('#auto-electrician_price').val(value);
                  break;
                
                case 'Буксировка': 
                  $('#towing-change').attr('checked', 'checked');
                  $('#towing_price').val(value);
                  break;
                
                case 'Эвакуатор': 
                  $('#tow-truck-change').attr('checked', 'checked');
                  $('#tow-truck_price').val(value);
                  break;
                
                case 'Взлом замков': 
                  $('#locks-breaking-change').attr('checked', 'checked');
                  $('#locks-breaking_price').val(value);
                  break;
              }
            });
          });
        });

        $(".overlay-add-close").on("click", function() {
          $(".prices-add").hide();
          $(".overlay").hide();
        });

        $(".overlay-search-close").on("click", function() {
          $(".overlay").hide();
        });

        $(".overlay-change-close").on("click", function() {
          $(".prices-change").hide();
          $(".overlay").hide();
        });

        $(".overlay-desc-close").on("click", function() {
          $(".overlay").hide();
        });

        $(".overlay-delete-close").on("click", function() {
          $(".overlay").hide();
        });

        $(".price-add").on("click", function() {
          $(".prices-add").show();
        });

        $(".save-add").on("click", function() {
          $(".prices-add").hide();
        });

        $(".price-change").on("click", function() {
          $(".prices-change").show();
        });

        $(".save-change").on("click", function() {
          $(".prices-change").hide();
        });

        $('.overlay-delete-form').submit(function(event) {
          event.preventDefault();
          $(".overlay").hide();

          $.ajax({
            type: 'POST',
            url: 'php/scripts/delete.php',
            data: {password: $('.overlay-delete-form .password').val(), id: record_id.replace(/[^\d]/g, '')}
          }).done(function(response) {
            $('form').trigger('reset');

            if (response == "true") {
              alert('Запись удалена');
              $("#record_" + record_id.replace(/[^\d]/g, '')).remove();
            }
            else
              alert('Неверный пароль!');
          });
          return false;
        });

        $('.overlay-desc-form').submit(function(event) {
          event.preventDefault();
          $(".overlay").hide();

          $.ajax({
            type: 'POST',
            url: 'php/scripts/change-description.php',
            data: {password: $('.overlay-desc-form .password').val(), text: $('.overlay-desc-form .description').val(), id: record_id.replace(/[^\d]/g, '')}
          }).done(function(response) {
            $('form').trigger('reset');

            if (response == "true")
              alert('Информация обновлена');
            else
              alert('Неверный пароль!');
          });
          return false;
        });



        $('.overlay-change-form').submit(function(event) {
          event.preventDefault();
          $(".overlay").hide();
          $('form').trigger('reset');

          $.ajax({
            type: 'POST',
            url: 'php/scripts/edit.php',
            data: {
              password: $('.overlay-change-form .password').val(), 
              surname: $('#change-surname').val(), 
              name: $('#change-name').val(), 
              patronymic: $('#change-patronymic').val(), 
              phone: $('.overlay-change-form .phone').val(), 
              wa_flag: $('.overlay-change-form .whatsapp').prop('checked'), 
              districts: $('.overlay-change-form .district').val(), 
              holidays: $('.overlay-change-form .holidays').val(), 
              time_start: $('.overlay-change-form .start').val(), 
              time_end: $('.overlay-change-form .end').val(), 
              tf_flag: $('#tire_fitting-change').prop('checked'),
              ptd_flag: $('#petrol-tire_delivery-change').prop('checked'),
              l_flag: $('#lighter-change').prop('checked'),
              ae_flag: $('#auto-electrician-change').prop('checked'),
              t_flag: $('#towing-change').prop('checked'),
              tt_flag: $('#tow-truck-change').prop('checked'),
              lb_flag: $('#locks-breaking-change').prop('checked'),
              tf_price: $('#tire_fitting_price').val(),
              ptd_price: $('#petrol-tire_delivery_price').val(),
              l_price: $('#lighter_price').val(),
              ae_price: $('#auto-electrician_price').val(),
              t_price: $('#towing_price').val(),
              tt_price: $('#tow-truck_price').val(),
              lb_price: $('#locks-breaking_price').val(),
              id: record_id.replace(/[^\d]/g, '')
            }
          }).done(function(response) {
            if (response == "true")
              alert('Запись изменена');
            else
              alert('Неверный пароль!');
          });
          return false;
        });

        $('.overlay-add-form').submit(function(event) {
          event.preventDefault();
          $(".overlay").hide();
          $.ajax({
            type: 'POST',
            url: 'php/scripts/add.php',
            data: $(this).serialize()
          }).done(function(response) {
            $('form').trigger('reset');

            if (response == "true")
              alert('Запись добавлена');
            else
              alert('Неверный пароль!');
          });
          return false;
        });

        $('.overlay-search-form').submit(function(event) {
          event.preventDefault();
          $(".overlay").hide();

          $.ajax({
            type: 'POST',
            url: 'php/scripts/search.php',
            data: $(this).serialize()
            }).done(function(response) {
              $('form').trigger('reset');
              $( ".record" ).remove();

              people = JSON.parse(response);
              if (!people.length == 0) {
                $.each(people, function(index, index){
                  if (index.wa == 1)
                    wa = '<div class="whatsapp"></div\>';
                  else
                    wa = '';
                    
                  $('.bar').after('<div class="row justify-content-between record" id="record_'+ index.id +'"\><div class="col-6 left"\><p class="executor">' + index.surname + ' ' + index.name + '&nbsp;' + '</p\><div class="buttons-wrap"><div class="edit" id="edit_'+ index.id +'" onclick=""></div>&nbsp\;<div class="text" id="text_'+ index.id +'" onclick=""></div>&nbsp\;<div class="delete" id="delete_'+ index.id +'" onclick=""></div\></div\><div class="wrap"\>'+ wa +'</div\></div\><div class="col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4 right"\><p class="phone">'+ index.phone +'</p\><div class="wrap"\><p class="rating">'+ index.rating +'</p>&nbsp;<div class="star-icon"></div\></div\></div\></div\>');
                });
              }
              else
                alert('Ничего не найдено!');
            });
          return false;
        }); 
      });
    </script>
  </body>
</html>