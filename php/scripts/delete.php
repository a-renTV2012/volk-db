<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/people.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/time.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/people_services.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/people_districts.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/comm_channels.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/price_list.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/districts.php');

$conn = new mysqli('localhost', 'root', '123');
$database = 'volk';

person::useConnection($conn, $database);
work_time::useConnection($conn, $database);
person_service::useConnection($conn, $database);
person_district::useConnection($conn, $database);
comm_channel::useConnection($conn, $database);
service::useConnection($conn, $database);
district::useConnection($conn, $database);

$log = fopen($_SERVER['DOCUMENT_ROOT'].'/php/logs/delete/log '.date("d.m.Y - H.i.s", time()).'.log', 'w+');

$pass = $_POST['password'];
$id = $_POST['id'];

fwrite($log, $id."\n\n");

if($pass == '123') {
  $person = person::retrieveByPK($id);

  fwrite($log, 'ФИО: '.$person->surname.' '.$person->name.' '.$person->patronymic."\n");

  $comm_channels = comm_channel::retrieveByField('person', $id);

  foreach ($comm_channels as $value) {
    if ($value->channel == 2)
      fwrite($log, 'Телефон: '.$value->value."\n\n");
    $value->delete();
  }

  fwrite($log, 'Услуги:'."\n");
  $services = person_service::retrieveByField('person', $id);

  foreach ($services as $value) {
    $service = service::retrieveByPK($value->service);
    fwrite($log, ' '.$service->service."\n");

    $value->delete();
  }

  fwrite($log, "\n".'Районы:'."\n");
  $districts = person_district::retrieveByField('person', $id);

  foreach ($districts as $value)
  {
    $district = district::retrieveByPK($value->district);
    fwrite($log, ' '.$district->district."\n");

    $value->delete();
  }

  $work_time = work_time::retrieveByField('person', $id);

  fwrite($log, "\n".'Время работы: '.$work_time[0]->time_start.' - '.$work_time[0]->time_end."\n");
  fwrite($log, 'Выходные:'."\n");

  if ($work_time[0]->monday == 1)
    fwrite($log, ' Понедельник'."\n");
  if ($work_time[0]->tuesday == 1)
    fwrite($log, ' Вторник'."\n");
  if ($work_time[0]->wednesday == 1)
    fwrite($log, ' Среда'."\n");
  if ($work_time[0]->thursday == 1)
    fwrite($log, ' Четверг'."\n");
  if ($work_time[0]->friday == 1)
    fwrite($log, ' Пятница'."\n");
  if ($work_time[0]->saturday == 1)
    fwrite($log, ' Суббота'."\n");
  if ($work_time[0]->sunday == 1)
    fwrite($log, ' Воскресенье'."\n");
  
  $work_time[0]->delete();
  $person->delete();
}
else {
  fwrite($log, 'Incorrect password!');
  fclose($log);
  exit();
}

fclose($log);

if ($pass == '123')
  echo "true";
else
  echo "false";
?>