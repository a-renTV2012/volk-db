<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/people.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/time.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/people_services.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/price_list.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/districts.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/people_districts.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/comm_channels.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/channels_names.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/positions.php');

$conn = new mysqli('localhost', 'root', '123');
$database = 'volk';

person::useConnection($conn, $database);
work_time::useConnection($conn, $database);
person_service::useConnection($conn, $database);
service::useConnection($conn, $database);
district::useConnection($conn, $database);
person_district::useConnection($conn, $database);
comm_channel::useConnection($conn, $database);
channel_name::useConnection($conn, $database);
position::useConnection($conn, $database);

$id = $_POST['id'];

$person = person::retrieveByPK($id);

$surname = $person->surname;
$name = $person->name;
$patronymic = $person->patronymic;

$position = position::retrieveByPK(1);

$channels = comm_channel::retrieveByField('person', $id);

$wa_flag = 0;
foreach ($channels as $value) {
  if (!$value->value == '')
    $phone = $value->value;
  else
    $wa_flag = 1;
}


$time = work_time::retrieveByField('person', $id);

$time_start = $time[0]->time_start;
$time_end = $time[0]->time_end;

$days_off = [];
if ($time[0]->monday == 1)
  $days_off[0] = 'Понедельник';
if ($time[0]->tuesday == 1)
  $days_off[1] = 'Вторник';
if ($time[0]->wednesday == 1)
  $days_off[2] = 'Среда';
if ($time[0]->thursday == 1)
  $days_off[3] = 'Четверг';
if ($time[0]->friday == 1)
  $days_off[4] = 'Пятница';
if ($time[0]->saturday == 1)
  $days_off[5] = 'Суббота';
if ($time[0]->sunday == 1)
  $days_off[6] = 'Воскресенье';

$holidays = '';
foreach ($days_off as $value)
  $holidays = $holidays.', '.$value;

$holidays = substr($holidays, 2);


$dists = person_district::retrieveByField('person', $id);

$districts = '';
foreach($dists as $value){
  $dist = district::retrieveByPK($value->district);
  $districts = $districts.', '.$dist->district;
}

$districts = substr($districts, 2);


$servs = person_service::retrieveByField('person', $id);

$services = [];
foreach ($servs as $value){
  $service = service::retrieveByPK($value->service);
  $services[$service->service] = $value->price;
}

$info = [
  'surname' => $surname,
  'name' => $name,
  'patronymic' => $patronymic,
  'phone' => $phone,
  'wa_flag' => $wa_flag,
  'time_start' => $time_start,
  'time_end' => $time_end,
  'holidays' => $holidays,
  'districts' => $districts,
  'services' => $services
];

echo json_encode($info);
?>