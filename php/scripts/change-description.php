<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/people.php');

$conn = new mysqli('localhost', 'root', '123');
$database = 'volk';

person::useConnection($conn, $database);

$log = fopen($_SERVER['DOCUMENT_ROOT'].'/php/logs/description-change/log '.date("d.m.Y - H.i.s", time()).'.log', 'w+');

$pass = $_POST['password'];
$id = $_POST['id'];
$text = $_POST['text'];

fwrite($log, $id."\n\n");

if ($pass == '123'){
  $person = person::retrieveByPK($id);

  fwrite($log, 'ФИО: '.$person->surname.' '.$person->name.' '.$person->patronymic."\n");
  fwrite($log, 'Дополнительная информация: '.$person->add_info."\n");

  $person->add_info = $text;

  fwrite($log, 'Изменена на: '.$text);

  $person->save();
}
else {
  fwrite($log, 'Incorrect password!');
  fclose($log);
  exit();
}

fclose($log);

if ($pass == '123')
  echo "true";
else
  echo "false";
?>