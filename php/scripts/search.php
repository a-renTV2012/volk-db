<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/people.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/people_services.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/people_districts.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/comm_channels.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/districts.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/time.php');

$conn = new mysqli('localhost', 'root', '123');
$database = 'volk';

person::useConnection($conn, $database);
person_service::useConnection($conn, $database);
person_district::useConnection($conn, $database);
comm_channel::useConnection($conn, $database);
district::useConnection($conn, $database);
work_time::useConnection($conn, $database);

$log = fopen($_SERVER['DOCUMENT_ROOT'].'/php/logs/search/log '.date("d.m.Y - H.i.s", time()).'.log', 'w+');

fwrite($log, 'Шаблон'."\n\n");
fwrite($log, 'Время: '.date('H:i', time())."\n");

$district = $_POST['district'];

$district = district::retrieveByField('district', mb_strtolower($district, 'UTF-8'));
$values = person_district::retrieveByField('district', $district[0]->id);

$people_0 = [];
for ($i = 0; $i < count($values); $i++)
  $people_0[$i] = $values[$i]->person;

fwrite($log, 'Район: '.$district[0]->district."\n\n");

fwrite($log, 'Услуги: '."\n");

$people_1 = [];
if (isset($_POST['tire_fitting'])) {
  fwrite($log, ' Шиномонтаж'."\n");

  $values = person_service::retrieveByField('service', 1);
  
  for ($i = 0; $i < count($values); $i++)
    $people_1[$i] = $values[$i]->person;
}

$people_2 = [];
if (isset($_POST['petrol-tire_delivery'])) {
  fwrite($log, ' Подвоз бензина и шин'."\n");

  $values = person_service::retrieveByField('service', 2);
  
  for ($i = 0; $i < count($values); $i++)
    $people_2[$i] = $values[$i]->person;
}

$people_3 = [];
if (isset($_POST['lighter'])) {
  fwrite($log, ' Прикуриватель'."\n");

  $values = person_service::retrieveByField('service', 3);
  
  for ($i = 0; $i < count($values); $i++)
    $people_3[$i] = $values[$i]->person;
}

$people_4 = [];
if (isset($_POST['auto-electrician'])) {
  fwrite($log, ' Автоэлектрик'."\n");

  $values = person_service::retrieveByField('service', 4);

  for ($i = 0; $i < count($values); $i++)
    $people_4[$i] = $values[$i]->person;
}

$people_5 = [];
if (isset($_POST['towing'])) {
  fwrite($log, ' Буксировка'."\n");

  $values = person_service::retrieveByField('service', 5);

  for ($i = 0; $i < count($values); $i++)
    $people_5[$i] = $values[$i]->person;
}

$people_6 = [];
if (isset($_POST['tow-truck'])) {
  fwrite($log, ' Эвакуатор'."\n");

  $values = person_service::retrieveByField('service', 6);

  for ($i = 0; $i < count($values); $i++)
    $people_6[$i] = $values[$i]->person;
}

$people_7 = [];
if (isset($_POST['locks-breaking'])) {
  fwrite($log, ' Взлом замков'."\n");

  $values = person_service::retrieveByField('service', 7);

  for ($i = 0; $i < count($values); $i++)
    $people_7[$i] = $values[$i]->person;
}

$people_8 = [];

/*$day_of_week = mb_strtolower(date('l', time()),'UTF-8');
$values = work_time::retrieveByField($day_of_week, NULL or 0);

for ($i = 0; $i < count($values); $i++) {
  if (strtotime($values[$i]->time_start) < time() and strtotime($values[$i]->time_end > time()))
    $people_8[$i] = $values[$i]->person;
}*/

if (count($people_1 == 0) and count($people_2 == 0) and count($people_3 == 0) and count($people_4 == 0) and count($people_5 == 0) and count($people_6 == 0) and count($people_7 == 0)) {
  fwrite($log, "\n".'Ничего не найдено!'); 
  $empty = [];
  echo json_encode($empty);
  exit();
}

fwrite($log, "\n\n\n\n".'Записи:'."\n\n");

$people_array = [];

$people_array[0] = $people_0;
$people_array[1] = $people_1;
$people_array[2] = $people_2;
$people_array[3] = $people_3;
$people_array[4] = $people_4;
$people_array[5] = $people_5;
$people_array[6] = $people_6;
$people_array[7] = $people_7;
$people_array[8] = $people_8;

$people_array = array_values(array_filter($people_array));

$people_ids = $people_array[0];
for ($i = 0; $i < count($people_array) - 1; $i++)
  $people_ids = array_intersect($people_ids, $people_array[$i + 1]);

$people = [];
$i = 0;
if (!count($people_ids) == 0) {
  foreach ($people_ids as $value) {
    $person = person::retrieveByPK($value);
    $channels = comm_channel::retrieveByField('person', $value);
    $time = work_time::retrieveByField('person', $value);

    foreach ($channels as $channel) {
      if ($channel->channel == 1)
        $wa_flag = 1;
      else
        $wa_flag = 0;
      if ($channel->channel == 2)
        $phone = $channel->value;
    }

    $people[$i] = [
      'surname' => $person->surname, 
      'name' => $person->name, 
      'patronymic' => $person->patronymic, 
      'id' => $person->id, 
      'rating' => $person->rating, 
      'wa' => $wa_flag, 
      'phone' => $phone
    ];

    $i++;

    fwrite($log, $person->id."\n".
    'Фамилия: '.$person->surname."\n".
    'Имя: '.$person->name."\n".
    'Отчество: '.$person->patronymic."\n".
    'Телефон: '.$phone."\n".
    'Использование WhatsApp: '.$wa_flag."\n".
    'Рейтинг: '.$person->rating."\n\n".
    'Время работы: '.$time[0]->time_start.' - '.$time[0]->time_end."\n"
    );
  }  
}

fwrite($log, "\n\n".'Ответ сервера: '.json_encode($people));
fclose($log);

echo json_encode($people);
?>