<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/people.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/time.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/people_services.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/price_list.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/districts.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/people_districts.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/comm_channels.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/channels_names.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/positions.php');

$conn = new mysqli('localhost', 'root', '123');
$database = 'volk';

person::useConnection($conn, $database);
work_time::useConnection($conn, $database);
person_service::useConnection($conn, $database);
service::useConnection($conn, $database);
district::useConnection($conn, $database);
person_district::useConnection($conn, $database);
comm_channel::useConnection($conn, $database);
channel_name::useConnection($conn, $database);
position::useConnection($conn, $database);

$log = fopen($_SERVER['DOCUMENT_ROOT'].'/php/logs/edit/log '.date("d.m.Y - H.i.s", time()).'.log', 'w+');

$pass = $_POST['password'];
$id = $_POST['id'];

fwrite($log, $id."\n\n");

if ($pass == '123') {
  $person = person::retrieveByPK($id);
  
  $surname = $_POST['surname'];
  $name = $_POST['name'];
  $patronymic = $_POST['patronymic'];
  
  $person->surname = $surname;
  $person->name = $name;
  $person->patronymic = $patronymic;
  fwrite($log, 'ФИО: '.$surname.' '.$name.' '.$patronymic."\n");
  
  $position = position::retrieveByPK(1);
  $person->position = $position->id;
  fwrite($log, 'Должность: '.$position->Position."\n");
  $person->save();

  $phone_record = comm_channel::retrieveByField('person', $id);

  foreach ($phone_record as $value)
    $value->delete();

  $phone = $_POST['phone'];
  $wa_flag = $_POST['wa_flag'];

  $wa_record = new comm_channel;
  $wa_record->channel = 2;
  $wa_record->person = $id;
  $wa_record->value = $phone;
  $wa_record->save();


  if ($wa_flag == 'true') {
    $phone_record = new comm_channel;
    $phone_record->channel = 1;
    $phone_record->person = $id;
    $phone_record->value = '';
    $phone_record->save();
    }
  
  fwrite($log, 'Телефон: '.$phone."\n\n");


  $time = work_time::retrieveByField('person', $id);

  $time_start = $_POST['time_start'];
  $time_end = $_POST['time_end'];
  $holidays = str_replace(' ', '', $_POST['holidays']);
  $days_off = explode(',', $holidays);
  
  $time[0]->time_start = $time_start;
  $time[0]->time_end = $time_end;

  fwrite($log, 'Выходные:'."\n");

  $time[0]->monday = 0;
  $time[0]->tuesday = 0;
  $time[0]->wednesday = 0;
  $time[0]->thursday = 0;
  $time[0]->friday = 0;
  $time[0]->saturday = 0;
  $time[0]->sunday = 0;

  foreach($days_off as $value) {
    $value = mb_strtolower($value, 'UTF-8');
    if ($value == 'понедельник')
      $time[0]->monday = 1;
    if ($value == 'вторник')
      $time[0]->tuesday = 1;
    if ($value == 'среда')
      $time[0]->wednesday = 1;
    if ($value == 'четверг')
      $time[0]->thursday = 1;
    if ($value == 'пятница')
      $time[0]->friday = 1;
    if ($value == 'суббота')
      $time[0]->saturday = 1;
    if ($value == 'воскресенье')
      $time[0]->sunday = 1;
    fwrite($log, ' '.$value."\n");
  }
  $time[0]->save();
  fwrite($log, 'Время работы: '.$time_start.' - '.$time_end."\n");


  $districts1 = str_replace(' ', '', $_POST['districts']);
  $districts = explode(',', $districts1);
  fwrite($log, 'Районы работы:'."\n");

  $dists = person_district::retrieveByField('person', $id);
  foreach ($dists as $value)
    $value->delete();

  foreach($districts as $value) {
    $dist = new person_district;
    $dist->person = $id;

    $value = mb_strtolower($value, 'UTF-8');

    if ($value == 'дзержинский')
      $dist_id = district::retrieveByPK(1);
    if ($value == 'заельцовский')
      $dist_id = district::retrieveByPK(2);
    if ($value == 'кировский')
      $dist_id = district::retrieveByPK(3);
    if ($value == 'октябрьский')
      $dist_id = district::retrieveByPK(4);
    if ($value == 'ленинский')
      $dist_id = district::retrieveByPK(5);
    if ($value == 'советский')
      $dist_id = district::retrieveByPK(6);
    if ($value == 'железнодорожный')
      $dist_id = district::retrieveByPK(7);
    if ($value == 'калининский')
      $dist_id = district::retrieveByPK(8);
    if ($value == 'первомайский')
      $dist_id = district::retrieveByPK(9);
    if ($value == 'центральный')
      $dist_id = district::retrieveByPK(10);
    if ($value == 'новосибирск') {
      $dist_id = [];
      for ($i = 1; $i < 11; $i++) {
        $dist_id[$i] = district::retrieveByPK($i);
        $dist_ = new person_district;
        $dist_->district = $dist_id[$i]->id;
        $dist_->person = $person->id;
        $dist_->save();
        fwrite($log, ' '.$dist_id[$i]->district."\n");
        unset($dist_);
      }
      break;
    }

    $dist->district = $dist_id->id;
    $dist->save();
    fwrite($log, ' '.$dist_id->district."\n");
  }
  fwrite($log, "\n");

  fwrite($log, 'Услуги:'."\n");

  $person_services = person_service::retrieveByField('person', $id);
  foreach ($person_services as $value)
    $value->delete();

  if ($_POST['tf_flag'] == "true") {
    $record = new person_service;
    $record->person = $id;
    $ser_name = service::retrieveByPK(1);
    $record->service = $ser_name->id;
    $record->price = $_POST['tf_price'];
    $record->save(); 
    fwrite($log, ' '.$ser_name->service.', '.$record->price.' руб'."\n");
  }
  if ($_POST['ptd_flag'] == "true") {
    $record = new person_service;
    $record->person = $id;
    $ser_name = service::retrieveByPK(2);
    $record->service = $ser_name->id;
    $record->price = $_POST['ptd_price'];
    $record->save();
    fwrite($log, ' '.$ser_name->service.', '.$record->price.' руб'."\n");
  }
  if ($_POST['l_flag'] == "true") {
    $record = new person_service;
    $record->person = $id;
    $ser_name = service::retrieveByPK(3);
    $record->service = $ser_name->id;
    $record->price = $_POST['l_price'];
    $record->save();
    fwrite($log, ' '.$ser_name->service.', '.$record->price.' руб'."\n");
  }
  if ($_POST['ae_flag'] == "true") {
    $record = new person_service;
    $record->person = $id;
    $ser_name = service::retrieveByPK(4);
    $record->service = $ser_name->id;
    $record->price = $_POST['ae_price'];
    $record->save();
    fwrite($log, ' '.$ser_name->service.', '.$record->price.' руб'."\n");
  }
  if ($_POST['t_flag'] == "true") {
    $record = new person_service;
    $record->person = $id;
    $ser_name = service::retrieveByPK(5);
    $record->service = $ser_name->id;
    $record->price = $_POST['t_price'];
    $record->save();
    fwrite($log, ' '.$ser_name->service.', '.$record->price.' руб'."\n");
  }
  if ($_POST['tt_flag'] == "true") {
    $record = new person_service;
    $record->person = $id;
    $ser_name = service::retrieveByPK(6);
    $record->service = $ser_name->id;
    $record->price = $_POST['tt_price'];
    $record->save();
    fwrite($log, ' '.$ser_name->service.', '.$record->price.' руб'."\n");
  }
  if ($_POST['lb_flag'] == "true") {
    $record = new person_service;
    $record->person = $id;
    $ser_name = service::retrieveByPK(7);
    $record->service = $ser_name->id;
    $record->price = $_POST['lb_price'];
    $record->save();
    fwrite($log, ' '.$ser_name->service.', '.$record->price.' руб'."\n");
  }
}
else {
  fwrite($log, 'Incorrect password!');
  fclose($log);
  exit();
}

fclose($log);

if ($pass == '123')
  echo "true";
else
  echo "false";
?>