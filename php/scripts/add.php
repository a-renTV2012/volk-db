<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/people.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/time.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/people_services.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/price_list.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/districts.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/people_districts.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/comm_channels.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/channels_names.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/models/positions.php');

$conn = new mysqli('localhost', 'root', '123');
$database = 'volk';

person::useConnection($conn, $database);
work_time::useConnection($conn, $database);
person_service::useConnection($conn, $database);
service::useConnection($conn, $database);
district::useConnection($conn, $database);
person_district::useConnection($conn, $database);
comm_channel::useConnection($conn, $database);
channel_name::useConnection($conn, $database);
position::useConnection($conn, $database);

$log = fopen($_SERVER['DOCUMENT_ROOT'].'/php/logs/add/log '.date("d.m.Y - H.i.s", time()).'.log', 'w+');

$pass = $_POST['password'];

if ($pass == '123') {
  $person = new person;
  
  $surname = $_POST['surname'];
  $name = $_POST['name'];
  $patronymic = $_POST['patronymic'];
  
  $person->surname = $surname;
  $person->name = $name;
  $person->patronymic = $patronymic;
  fwrite($log, 'ФИО: '.$surname.' '.$name.' '.$patronymic."\n");
  
  $position = position::retrieveByPK(1);
  $person->position = $position->id;
  fwrite($log, 'Должность: '.$position->Position."\n");
  $person->save();

  $phone_record = new comm_channel;

  $phone = $_POST['phone'];
  
  $phone_record->person = $person->id;
  $cName = channel_name::retrieveByPK(2);
  $phone_record->channel = $cName->id;
  $phone_record->value = $phone;
  $phone_record->save();
  fwrite($log, 'Телефон: '.$phone."\n\n");

  if (isset($_POST['whatsapp'])) {
  $whatsapp = $_POST['whatsapp'];

    if ($whatsapp == 'on') {
      $record = new comm_channel;
      $record->person = $person->id;
      $wa_name = channel_name::retrieveByPK(1);
      $record->channel = $wa_name->id;
      $record->save();
      unset($record);
    }
  }

  $time = new work_time;

  $time_start = $_POST['time_start'];
  $time_end = $_POST['time_end'];
  $holidays = str_replace(' ', '', $_POST['holidays']);
  $days_off = explode(',', $holidays);
  
  $time->person = $person->id;
  $time->time_start = $time_start;
  $time->time_end = $time_end;

  fwrite($log, 'Выходные:'."\n");
  foreach($days_off as $value) {
    $value = mb_strtolower($value, 'UTF-8');
    if ($value == 'понедельник')
      $time->monday = 1;
    if ($value == 'вторник')
      $time->tuesday = 1;
    if ($value == 'среда')
      $time->wednesday = 1;
    if ($value == 'четверг')
      $time->thursday = 1;
    if ($value == 'пятница')
      $time->friday = 1;
    if ($value == 'суббота')
      $time->saturday = 1;
    if ($value == 'воскресенье')
      $time->sunday = 1;
    fwrite($log, ' '.$value."\n");
  }
  unset($value);
  $time->save();
  fwrite($log, 'Время работы: '.$time_start.' - '.$time_end."\n");

  $districts1 = str_replace(' ', '', $_POST['district']);
  $districts = explode(',', $districts1);
  fwrite($log, 'Районы работы:'."\n");
  foreach($districts as $value) {
    $dist = new person_district;
    $dist->person = $person->id;

    $value = mb_strtolower($value, 'UTF-8');

    if ($value == 'дзержинский')
      $dist_id = district::retrieveByPK(1);
    if ($value == 'заельцовский')
      $dist_id = district::retrieveByPK(2);
    if ($value == 'кировский')
      $dist_id = district::retrieveByPK(3);
    if ($value == 'октябрьский')
      $dist_id = district::retrieveByPK(4);
    if ($value == 'ленинский')
      $dist_id = district::retrieveByPK(5);
    if ($value == 'советский')
      $dist_id = district::retrieveByPK(6);
    if ($value == 'железнодорожный')
      $dist_id = district::retrieveByPK(7);
    if ($value == 'калининский')
      $dist_id = district::retrieveByPK(8);
    if ($value == 'первомайский')
      $dist_id = district::retrieveByPK(9);
    if ($value == 'центральный')
      $dist_id = district::retrieveByPK(10);
    if ($value == 'новосибирск') {
      $dist_id = [];
      for ($i = 1; $i < 11; $i++) {
        $dist_id[$i] = district::retrieveByPK($i);
        $dist_ = new person_district;
        $dist_->district = $dist_id[$i]->id;
        $dist_->person = $person->id;
        $dist_->save();
        fwrite($log, ' '.$dist_id[$i]->district."\n");
        unset($dist_);
      }
      break;
    }

    $dist->district = $dist_id->id;
    $dist->save();
    unset($dist);
    fwrite($log, ' '.$dist_id->district."\n");
    unset($dist_id);
  }
  unset($value);
  fwrite($log, "\n");

  fwrite($log, 'Услуги:'."\n");
  if (isset($_POST['tire-fitting']) and isset($_POST['tire_fitting_price'])) {
    $record = new person_service;
    $record->person = $person->id;
    $ser_name = service::retrieveByPK(1);
    $record->service = $ser_name->id;
    $record->price = $_POST['tire_fitting_price'];
    $record->save(); 
    fwrite($log, ' '.$ser_name->service.', '.$record->price.' руб'."\n");
    unset($record);
  }
  if (isset($_POST['petrol-tire_delivery']) and isset($_POST['petrol-tire_delivery_price'])) {
    $record = new person_service;
    $record->person = $person->id;
    $ser_name = service::retrieveByPK(2);
    $record->service = $ser_name->id;
    $record->price = $_POST['petrol-tire_delivery_price'];
    $record->save();
    fwrite($log, ' '.$ser_name->service.', '.$record->price.' руб'."\n");
    unset($record);
  }
  if (isset($_POST['lighter']) and isset($_POST['lighter_price'])) {
    $record = new person_service;
    $record->person = $person->id;
    $ser_name = service::retrieveByPK(3);
    $record->service = $ser_name->id;
    $record->price = $_POST['lighter_price'];
    $record->save();
    fwrite($log, ' '.$ser_name->service.', '.$record->price.' руб'."\n");
    unset($record);
  }
  if (isset($_POST['auto-electrician']) and isset($_POST['auto-electrician_price'])) {
    $record = new person_service;
    $record->person = $person->id;
    $ser_name = service::retrieveByPK(4);
    $record->service = $ser_name->id;
    $record->price = $_POST['auto-electrician_price'];
    $record->save();
    fwrite($log, ' '.$ser_name->service.', '.$record->price.' руб'."\n");
    unset($record);
  }
  if (isset($_POST['towing']) and isset($_POST['towing_price'])) {
    $record = new person_service;
    $record->person = $person->id;
    $ser_name = service::retrieveByPK(5);
    $record->service = $ser_name->id;
    $record->price = $_POST['towing_price'];
    $record->save();
    fwrite($log, ' '.$ser_name->service.', '.$record->price.' руб'."\n");
    unset($record);
  }
  if (isset($_POST['tow-truck']) and isset($_POST['tow-truck_price'])) {
    $record = new person_service;
    $record->person = $person->id;
    $ser_name = service::retrieveByPK(6);
    $record->service = $ser_name->id;
    $record->price = $_POST['tow-truck_price'];
    $record->save();
    fwrite($log, ' '.$ser_name->service.', '.$record->price.' руб'."\n");
    unset($record);
  }
  if (isset($_POST['locks-breaking']) and isset($_POST['locks-breaking_price'])) {
    $record = new person_service;
    $record->person = $person->id;
    $ser_name = service::retrieveByPK(7);
    $record->service = $ser_name->id;
    $record->price = $_POST['locks-breaking_price'];
    $record->save();
    fwrite($log, ' '.$ser_name->service.', '.$record->price.' руб'."\n");
    unset($record);
  }
}
else {
  fwrite($log, 'Incorrect password!');
  fclose($log);
  exit();
}

fclose($log);

if ($pass == '123')
  echo "true";
else
  echo "false";
?>