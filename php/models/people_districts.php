<?php

require_once('./../SimpleORM/SimpleOrm.class.php');

class person_district extends SimpleOrm {
  public static 
    $database = 'volk',
    $table = 'People_districts',
    $pk = 'id';

  public
    $person = 'person',
    $district = 'district';
}

?>