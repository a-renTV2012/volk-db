<?php

require_once('./../SimpleORM/SimpleOrm.class.php');

class channel_name extends SimpleOrm {
  public static 
    $database = 'volk',
    $table = 'Comm_channels_names',
    $pk = 'id';

  public
    $name = 'name';
}

?>