<?php

require_once('./../SimpleORM/SimpleOrm.class.php');

class work_time extends SimpleOrm {
  public static 
    $database = 'volk',
    $table = 'Time',
    $pk = 'id';

  public
    $person = 'person',
    $time_start = 'time_start',
    $time_end = 'time_end',
    $monday = 'monday',
    $tuesday = 'tuesday',
    $wednesday = 'wednesday',
    $thuesday = 'thuesday',
    $friday = 'friday',
    $saturday = 'saturday',
    $sunday = 'sunday';
}

?>