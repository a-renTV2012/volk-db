<?php

require_once('./../SimpleORM/SimpleOrm.class.php');

class car_model extends SimpleOrm {
  public static 
    $database = 'volk',
    $table = 'Car_models',
    $pk = 'id';
  
  public
    $model = 'model';
}

?>