<?php

require_once('./../SimpleORM/SimpleOrm.class.php');

class person_service extends SimpleOrm {
  public static 
    $database = 'volk',
    $table = 'People_services',
    $pk = 'id';

  public
    $person = 'person',
    $service = 'service',
    $price = 'price';
}

?>