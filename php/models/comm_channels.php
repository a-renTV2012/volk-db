<?php

require_once('./../SimpleORM/SimpleOrm.class.php');

class comm_channel extends SimpleOrm {
  public static 
    $database = 'volk',
    $table = 'Comm_channels',
    $pk = 'id';
    
  public
    $person = 'person',
    $channel = 'channel',
    $value = 'value';
}

?>