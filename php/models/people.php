<?php

require_once('./../SimpleORM/SimpleOrm.class.php');

class person extends SimpleOrm {
  public static 
    $database = 'volk',
    $table = 'People',
    $pk = 'id';

  public 
    $surname = 'surname',
    $name = 'name',
    $patronymic = 'patronymic',
    $position = 'position',
    $info = 'add_info',
    $rating = 'rating';
}

?>