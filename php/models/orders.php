<?php

require_once('./../SimpleORM/SimpleOrm.class.php');

class order extends SimpleOrm {
  public static 
    $database = 'volk',
    $table = 'Orders',
    $pk = 'id';

  public
    $executor = 'executor',
    $client = 'client',
    $time = 'time',
    $car_model = 'car_model',
    $description = 'order_description',
    $report = 'report',
    $mark = 'mark';
}

?>